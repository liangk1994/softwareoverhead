# Project Title

Containerised Internet Measurements

## Getting Started

In this project we aim to collect RTT value with

### Prerequisites

What things you need to install the software and how to install them
docker: https://docs.docker.com/install/
docker-compose: https://docs.docker.com/compose/install/

```
Give examples
```

## Running the tests

In order to start data collection, first identify which destination you would want to probe to, then create a name of the directory that the data is store. And then simply run:

```
sh commands_docker.sh {NAME_OF_DIRECTORY} {DESTINATION_ADDRESS}
```
or
```
sh commands_local.sh {NAME_OF_DIRECTORY} {DESTINATION_ADDRESS}
```

## For more information please refer to:

* [Report](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Presentation](https://maven.apache.org/) - Dependency Management


