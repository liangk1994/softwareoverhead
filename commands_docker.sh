# !/bin/bash
#Usage: sh commands_local.sh {NAME_OF_DIRECTORY} {DESTINATION_ADDRESS}

mkdir ./data/docker/$1
mkdir ./data/docker/$1/2_containers
mkdir ./data/docker/$1/10_containers
mkdir ./data/docker/$1/20_containers
mkdir ./data/docker/$1/40_containers
mkdir ./data/docker/$1/100_containers
mkdir ./data/docker/$1/200_containers

top -b -n 502 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=500 DES=$2 docker-compose up --no-color --scale ping=2 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/2_containers/
kill $(pidof top)

top -b -n 105 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=100 DES=$2 docker-compose up --no-color --scale ping=10 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/10_containers/
kill $(pidof top)

top -b -n 72 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=50 DES=$2 docker-compose up --no-color --scale ping=20 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/20_containers/
kill $(pidof top)

top -b -n 70 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=25 DES=$2 docker-compose up --no-color --scale ping=40 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/40_containers/
kill $(pidof top)

top -b -n 120 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=10 DES=$2 docker-compose up --no-color --scale ping=100 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/100_containers/
kill $(pidof top)

top -b -n 220 -d 1 -i > raw_usage_data.txt & sleep 1; COMPOSE_HTTP_TIMEOUT=120 NUM=5 DES=$2 docker-compose up --no-color --scale ping=200 > raw_data.txt
cp raw_usage_data.txt raw_data.txt ./data/docker/$1/200_containers/
kill $(pidof top)

rm raw_data.txt raw_usage_data.txt
touch raw_data.txt raw_usage_data.txt










