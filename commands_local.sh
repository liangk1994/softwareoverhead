#!/bin/bash
#Usage: sh commands_local.sh {NAME_OF_DIRECTORY} {DESTINATION_ADDRESS}

mkdir ./data/local/$1
mkdir ./data/local/$1/2_processes
mkdir ./data/local/$1/10_processes
mkdir ./data/local/$1/20_processes
mkdir ./data/local/$1/40_processes
mkdir ./data/local/$1/100_processes
mkdir ./data/local/$1/200_processes

top -b -n 502 -d 1 -i > raw_usage_data.txt & sh local.sh 2 500 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/2_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt

top -b -n 102 -d 1 -i > raw_usage_data.txt & sh local.sh 10 100 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/10_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt

top -b -n 52 -d 1 -i > raw_usage_data.txt & sh local.sh 20 50 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/20_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt

top -b -n 27 -d 1 -i > raw_usage_data.txt & sh local.sh 40 25 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/40_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt

top -b -n 20 -d 1 -i > raw_usage_data.txt & sh local.sh 100 10 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/100_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt

top -b -n 20 -d 1 -i > raw_usage_data.txt & sh local.sh 200 5 $2
cp raw_usage_data.txt raw_data.txt ./data/local/$1/200_processes/
kill $(pidof top)
rm raw_data.txt 
touch raw_data.txt









