#!/bin/bash

cd Mar.10th_EUG_XS/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.10th_HKCSL/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.10th_JPN_SOFTETHER/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.10th_NYC_ATT/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.10th_ROUTER/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.11th_EUG_XS/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.11th_HKCSL/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.11th_JPN_SOFTETHER/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.11th_NYC_ATT/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../

cd Mar.11th_ROUTER/
cd 2_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 10_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 20_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 40_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 100_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd 200_containers/
cat raw_data.txt | grep 'min/avg/max' > RTT_data.csv
cat raw_data.txt | grep 'packets' > packet_check.csv
cd ../
cd ../


