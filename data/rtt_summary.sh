#!/bin/bash
#Usage: sh usage_data_parsing.sh

echo "docker"
for directory in docker/*; do
	echo "$directory"
	for sub_directory in "$directory"/*; do
		echo "$sub_directory"
		cat "$sub_directory"/raw_data.txt | grep min/avg/max 
	done
done

echo "local"
for directory in local/*; do
	echo "$directory"
	for sub_directory in "$directory"/*; do
		echo "$sub_directory"
		cat "$sub_directory"/raw_data.txt | grep min/avg/max 
	done
done
