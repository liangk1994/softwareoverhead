#!/bin/bash
#Usage: sh usage_data_parsing.sh

echo "docker"
for directory in docker/Mar.1*; do
	echo "$directory"
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory"
		cat "$sub_directory"/raw_data.txt | grep time=
	done
done

echo "local"
for directory in local/Mar.1*; do
	echo "$directory"
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory"
		cat "$sub_directory"/raw_data.txt | grep time=
	done
done
